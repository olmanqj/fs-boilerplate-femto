#  Flight Software Boilerplate `femto` edition

This is a boiler plate project to quick start your flight software project, 
It has many things pre-configured, so you don't have to wast time on the
boilerplate code.


## Features
The following features are provided to be used out of the box within your
flight software project.
- Command Line Interface (CLI)
- State Machine
- Parameter Service
- Telecommands
- Telemetry



## Requirements
To support this boilerplate project, is expected to have:
- TBD

## Usage

TBD
