// BSP includes
#include "Arduino.h"
// Project includes
#include "s3/param.h"

// Reference to root param table
extern param_table_t root_table;

char param_val_buff[64];

void print_param_table()
{
  int idx=0;
  param_ref_t* ref;
  param_iterator_t iter = get_table_interator(&root_table);
  
  
  sprintf(param_val_buff, "%-4s | %-25s | %s", "Idx", "Param", "Value");
  Serial.println(param_val_buff);

  Serial.println("=========================================================");

  while((ref=table_interator_advance(&iter)))
  {
    sprintf(param_val_buff, "%-4d | %-25s | ", idx++, pref_get_full_name(ref));
    pref_to_str(ref, param_val_buff+strlen(param_val_buff), sizeof(param_val_buff)-strlen(param_val_buff));
    Serial.println(param_val_buff);
  }
}