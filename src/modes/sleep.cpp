#include "s3/param.h"
#include "s3/statem.h"
#include "modes.hpp"


int  safe_routine(state_ctx_t* ctx)
{
    return EXIT_SUCCESS;
}


extern paramU8_t     tlm_ena;

int safe_on_resume(state_ctx_t*)
{
    setUINT8(&tlm_ena, 0);
    return EXIT_SUCCESS;
}


int safe_on_pause(state_ctx_t*)
{
    setUINT8(&tlm_ena, 1);
    return EXIT_SUCCESS;
}