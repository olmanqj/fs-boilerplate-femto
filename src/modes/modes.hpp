#pragma once

#include "s3/statem.h"

/// State Routines
int  idle_routine(state_ctx_t* ctx);
int  science_routine(state_ctx_t* ctx);
int  self_test_routine(state_ctx_t* ctx);
int  safe_routine(state_ctx_t* ctx);

/// Transition Routines


/// On Resume Routines
int safe_on_resume(state_ctx_t*);



/// On Pause Routines
int safe_on_pause(state_ctx_t*);