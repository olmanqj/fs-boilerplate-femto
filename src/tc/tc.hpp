/**
 * @file tc.h
 * 
 * Tele-Commands
 * 
 * 
 * 
 */


#pragma once



#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>



typedef enum
{
    TC_SUCCESS,
    TC_FAILURE
} tc_status_t;


typedef struct __attribute__((packed))
{
    uint8_t tc_code;
    uint8_t b1;
    uint8_t b2;
    uint8_t b3;
    uint8_t b4;
    uint8_t b5;
    uint8_t b6;
} tc_ctx_t;


/**
 * @brief Utility functions to convert 2, 3 or 4 bytes into  uint16_t or uint32_t
 * 
 * Result in the form ab or abcd 
 *
 */
uint16_t combine2uint16(uint8_t a, uint8_t b);
uint32_t combine2uint32(uint8_t a, uint8_t b, uint8_t c, uint8_t d);


/**
 * @brief  Function definition of Tele-Command
 *  
 */
typedef tc_status_t (*tc_function_t)(tc_ctx_t* ctx);


typedef struct
{
    uint16_t          code;
    tc_function_t     fn;
} tc_def_t;



typedef tc_def_t tc_table_t[];

/**
 * @brief Store a character in the Tele-Command buffer
 * 
 * @param new_char 
 */
void tc_read_char(uint8_t new_char);

/**
 * @brief Parse the Tele-Command buffer and execute the command
 * 
 * @param table 
 * @param table_size 
 * @return tc_status_t 
 */
tc_status_t tc_process(const tc_table_t table, size_t table_size);

