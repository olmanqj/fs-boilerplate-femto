#include "Arduino.h"
#include <assert.h>

#include "tc.hpp"

#define TC_COMMAND_HEADER_POS    0
#define TC_COMMAND_CODE_POS      1
#define TC_COMMAND_FOOTER_POS    8


#define TC_COMMAND_HEADER_CHAR   0xE0
#define TC_COMMAND_FOOTER_CHAR   0xED



uint8_t tc_buff[9];
size_t tc_buff_idx;


uint16_t combine2uint16(uint8_t a, uint8_t b)
{
    return ((uint16_t)a << 8) | (uint16_t)b;
}


uint32_t combine2uint32(uint8_t a, uint8_t b, uint8_t c, uint8_t d)
{
    return ((uint32_t)a << 24) | ((uint32_t)b << 16) | ((uint32_t)c << 8) | (uint32_t)d;
}



// Insert as ring-buffer
void tc_read_char(uint8_t new_char)
{
    tc_buff[tc_buff_idx % sizeof(tc_buff)] = new_char;
    tc_buff_idx++;
}

void _tc_clear_buffer()
{
    tc_buff_idx = 0;
    memset(tc_buff, 0, sizeof(tc_buff));
}


tc_status_t tc_process(const tc_table_t table, size_t table_size)
{
    tc_status_t status;
    size_t      idx;
    tc_ctx_t    ctx;
    const tc_def_t*   tc = NULL;

    assert(table);
    if(table == NULL)  return TC_FAILURE;

    // Check Header and Footer
    if(tc_buff[TC_COMMAND_HEADER_POS] != TC_COMMAND_HEADER_CHAR
       || tc_buff[TC_COMMAND_FOOTER_POS] != TC_COMMAND_FOOTER_CHAR)
    {
        return TC_FAILURE;
    }

    // Construct the context struct
    ctx.tc_code = tc_buff[TC_COMMAND_CODE_POS];
    ctx.b1 = tc_buff[2];
    ctx.b2 = tc_buff[3];
    ctx.b3 = tc_buff[4];
    ctx.b4 = tc_buff[5];
    ctx.b5 = tc_buff[6];
    ctx.b6 = tc_buff[7];

    // Find the corrsponding Tc in table
    for(idx=0; idx < table_size; idx++)
    {
        if(table[idx].code == ctx.tc_code)
            tc = &table[idx];
    }

    if(tc == NULL)
    {
        _tc_clear_buffer();
#ifdef DEBUG
        Serial.println("[TC]> Error: Unknown Tele-Command received!");
#endif
        return TC_FAILURE;
    }

    if(tc->fn == NULL)
    {
        _tc_clear_buffer();
#ifdef DEBUG
        Serial.println("[TC]> Error: Tele-Command without a function!");
#endif
        return TC_FAILURE;
    }

    // Run the TC
    status = tc->fn(&ctx);

#ifdef DEBUG
    if(status != TC_SUCCESS)
        Serial.println("[TC]> Error: Tele-Command execution failed!");
#endif

#ifdef DEBUG
        Serial.println("[TC]> Ok!");
#endif
   _tc_clear_buffer();
    return status;
}
