// BSP includes
#include "Arduino.h"
// External includes
#include "embedded_cli.h"
#include "s3/param.h"
#include "s3/statem.h"
// Project includes
#include "sys/sys_calls.h"
#include "stop/stop.h"
#include "tc/tc.hpp"
#include "config.h"


extern stop_runner_t stop_runner;
extern paramU8_t  sys_mode;



tc_status_t tc_f_dummy(tc_ctx_t* ctx)
{
    Serial.println("Hello Universe, I'm Poas Board! Made with <3 in Costa Rica!");
    return TC_SUCCESS;
}


tc_status_t tc_f_idle_mode(tc_ctx_t* ctx)
{
    setUINT8(&sys_mode, Idle);
    return TC_SUCCESS;
}


tc_status_t tc_f_scientific_mode(tc_ctx_t* ctx)
{
    setUINT8(&sys_mode, Science);
    return TC_SUCCESS;
}