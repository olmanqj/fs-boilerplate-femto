// BSP includes
#include <assert.h>
#include "Arduino.h"
// External includes
#include "embedded_cli.h"
#include "s3/param.h"
#include "s3/statem.h"
// Project includes
#include "stop/stop.h"
#include "tc/tc.hpp"
#include "utils/param_utils.hpp"
#include "config.h"


extern param_table_t root_table;



tc_status_t tc_f_param_table(tc_ctx_t* ctx)
{
    print_param_table();
    return TC_SUCCESS;
}

tc_status_t tc_f_set_param(tc_ctx_t* ctx)
{
    assert(ctx);
    uint16_t index = combine2uint16(ctx->b1, ctx->b2);
    uint32_t value =  combine2uint32(ctx->b3, ctx->b4, ctx->b5, ctx->b6);
    param_ref_t* pref = lookup_table(&root_table,  index);
    if(pref == NULL) return TC_FAILURE;
    pref_decode(pref, &value, sizeof(value));
    return TC_SUCCESS;
}


tc_status_t tc_f_get_param(tc_ctx_t* ctx)
{
    char buff[24];
    assert(ctx);
    uint16_t index = combine2uint16(ctx->b1, ctx->b2);
    param_ref_t* pref = lookup_table(&root_table,  index);
    if(pref == NULL) return TC_FAILURE;
    pref_to_str(pref, buff, sizeof(buff));
    Serial.println(buff);
    Serial.flush();
    return TC_SUCCESS;
}
