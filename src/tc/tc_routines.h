#pragma once

#include "tc/tc.hpp"


tc_status_t tc_f_dummy(tc_ctx_t* ctx);
tc_status_t tc_f_idle_mode(tc_ctx_t* ctx);
tc_status_t tc_f_scientific_mode(tc_ctx_t* ctx);
tc_status_t tc_f_param_table(tc_ctx_t* ctx);
tc_status_t tc_f_set_param(tc_ctx_t* ctx);
tc_status_t tc_f_get_param(tc_ctx_t* ctx);
