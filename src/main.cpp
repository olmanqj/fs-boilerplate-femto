// BSP Includes
#include <Arduino.h>
// External includes
#include "embedded_cli.h"
//#include <arduino-timer.h> // TODO timer hal
#include <s3/param.h>
#include <s3/statem.h>
// Project Includes
#include "cli/cli.h"
#include "modes/modes.hpp"
#include "telemetry/beacon.h"
#include "sys/sys_calls.h"
#include "stop/stop.h"
#include "tc/tc.hpp"
#include "config.h"
#include "state_table.h"
#include "tc_table.hpp"





/******************************************************************************
 * Global Instances
*******************************************************************************/
// State machine instance
state_machine_t state_machine;

// STOP runner (for running long procedures)
stop_runner_t stop_runner;

// Command Line Inteface Instance
EmbeddedCli*  serial_cli;

// Timer for task emulation
// Timer<> timer = timer_create_default(); // TODO timer hal

// Reference to root param table
extern param_table_t root_table;


/******************************************************************************
 * Used Params
*******************************************************************************/
extern paramU32_t tlm_period;

/******************************************************************************
 * Functions
*******************************************************************************/
void setup()
{
  int status;
 
  // Init GPIO
  init_GPIO();

  // Init Serial Ifaces
  Serial.begin(CONF_CLI_SERIAL_BAUD_RATE); 

  // Output greeting
	Serial.println("======================================================");
	Serial.print(CONF_APP_NAME);
  Serial.print(CONF_APP_VERSION);
  Serial.println(F(" build "  __DATE__  __TIME__ "\n"));

	// Mount the Parameter Table
	if(mount_param_table(&root_table)!=EXIT_SUCCESS)
	{
		Serial.println(("[MAIN]> Param Table mounted FAIL!"));
    on_fatal_failure();
	}

  if ((status = cli_init(&serial_cli)) != EXIT_SUCCESS)
  {
    Serial.print(F("Error: CLI init failed with exit status: "));
    Serial.println(status);
    on_fatal_failure();
  }

  // Init State Machine
  if ((status = state_machine_init(&state_machine, 
                                   state_table, 
                                   state_table_size, 
                                   Idle)) != EXIT_SUCCESS)
  {
    Serial.print(F("Error: STATE MACHINE init failed with exit status: "));
    Serial.println(status);
  }
  
  // Init STOP runner
  stop_runner_init(&stop_runner);

  // Init beacon timer
  // init_beacon_task(timer, getUINT32(&tlm_period)); // TODO port timer
}







void loop()
{
  int new_char;

////////////////////////////////////////////////////////////////////////////////
//(1) Tick the task timer: the FS uses a software timer to run tasks 
//    ( transmission of the beacon) on defined intervals of time.

  // timer.tick(); // TODO port timer


////////////////////////////////////////////////////////////////////////////////
// (2) Read characters from the Serial interface and pass it to the CLI or  
//     Command Dispatch.
  while (Serial.available() > 0) 
  {
    new_char = Serial.read();
    // Pass the new char to CLI instance
    embeddedCliReceiveChar(serial_cli, new_char);
  }
   

////////////////////////////////////////////////////////////////////////////////
// (3) Execute received commands.
  // Process the messages with CLI instance
    embeddedCliProcess(serial_cli);
  

////////////////////////////////////////////////////////////////////////////////
// (4) Run a cycle of current Mode of Operation via the State Machine
  state_machine_run(&state_machine);


////////////////////////////////////////////////////////////////////////////////
// (5) Run a cycle of active Test or Operation Procedure (STOP) routine, 
//     if any.
  stop_runner_work(&stop_runner);

}