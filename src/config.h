#ifndef CONFIG_H_
#define CONFIG_H_


enum state_e
{
  Idle          = 0x01,
  Self_test     = 0x04,
  Science       = 0x05,
  Sleep         = 0x06
};

#define CONF_APP_NAME                          "fs-boilerplate"
#define CONF_APP_VERSION                       "0.0.0"

#define CONF_CLI_SERIAL_BAUD_RATE               9600

#endif // CONFIG_H_