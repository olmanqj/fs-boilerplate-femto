

#include "s3/param.h"



#ifdef __cplusplus
#define EXTERNC extern "C"
#else
#define EXTERNC
#endif

EXTERNC int on_sys_mode_param_change();


EXTERNC int on_tlm_period_param_change();


#undef EXTERNC

