// BSP includes
#include "Arduino.h"
// External includes
//#include "arduino-timer.h"
#include "s3/param.h"
// Project includes
#include "s3/statem.h"
#include "telemetry/beacon.h"
#include "param_callbacks.h"



extern state_machine_t  state_machine;
extern paramU8_t        sys_mode;


int on_sys_mode_param_change()
{
#ifdef DEBUG
    Serial.print("[STATEM] Mode of Operation changed to: ");
    Serial.println(getUINT8(&sys_mode));
#endif
    state_machine_set_state(&state_machine, getUINT8(&sys_mode));
    return EXIT_SUCCESS;
}




extern paramU8_t  tlm_ena;
extern paramU32_t tlm_period;
//extern Timer<> timer;


int on_tlm_period_param_change()
{

#ifdef DEBUG
    Serial.print("[TLM] Beacon period changed to: ");
    Serial.println(getUINT32(&tlm_period));
#endif
    //resume_beacon_task(timer, getUINT32(&tlm_period)); // TODO port timer

    return EXIT_SUCCESS;
}