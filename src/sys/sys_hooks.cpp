/**
 * @file sys_hooks.cpp
 * @author olmanqj
 * @brief This file contains different hooks and callbacks required by the
 * system.
 * @date 2023-07-31
 */

#include "Arduino.h"

// Software reset function
void(* reset_sw) (void) = 0; 





void on_recoverable_failure(void) // TODO
{ 
    Serial.println("\nRecoverable Failure! Restarting...");
    delay(1000);
    reset_sw();
}



void on_fatal_failure(void) // TODO
{
    Serial.println("\nFatal Failure! Exiting...");
    delay(1000);
    reset_sw();
}