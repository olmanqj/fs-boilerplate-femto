/**
 * This files contains miscellaneous definitons concerning 
 * system initialization, callbacks and other functions.
 */


/**
 * @brief Initialise GPIO
 * 
 */
void init_GPIO(void);



/**
 * @brief On Fatal Failure Callback
 * 
 */
void on_fatal_failure(void);

