// BSP includes
#include <Arduino.h>
// External includes
#include "embedded_cli.h"
// Project includes
#include "cli/cli.h"
// Configs
#include "cli/conf_cli.h"

// Sys commands
extern CliCommandBinding cli_cmd_def_led;
extern CliCommandBinding cli_cmd_def_exit_cli;

// Param commands
extern CliCommandBinding cli_cmd_def_param_table;
extern CliCommandBinding cli_cmd_def_set_param;
extern CliCommandBinding cli_cmd_def_get_param;

// TNC commands
extern CliCommandBinding cli_cmd_def_tnc_config;
extern CliCommandBinding cli_cmd_def_tnc_command;
extern CliCommandBinding cli_cmd_def_tnc_echo;

// APRS commands 
extern CliCommandBinding cli_cmd_def_aprs_downlink;



//CLI_UINT cli_buffer[BYTES_TO_CLI_UINTS(CLI_BUFFER_SIZE)];

void cli_on_unkown_cmd(EmbeddedCli *cli, CliCommand *command);
void cli_write_char(EmbeddedCli *cli, char c);



int cli_init(EmbeddedCli**  cli)
{
    EmbeddedCliConfig *config = embeddedCliDefaultConfig();
    // config->cliBuffer           = cli_buffer;
    // config->cliBufferSize       = CLI_BUFFER_SIZE;
    // config->rxBufferSize        = CLI_RX_BUFFER_SIZE;
    // config->cmdBufferSize       = CLI_CMD_BUFFER_SIZE;
    config->historyBufferSize    = 0;//CLI_HISTORY_SIZE;
    config->invitation = "> ";
    config->enableAutoComplete   = false;
    config->maxBindingCount      = 13;//CLI_BINDING_COUNT;  /// <<< Set here the required number of commands!
    *cli = embeddedCliNew(config);
    if (*cli == NULL)
    {
        return EXIT_FAILURE;
    }
    (*cli)->onCommand = cli_on_unkown_cmd;
    (*cli)->writeChar = cli_write_char;

    // Add Command definitions to CLI
    embeddedCliAddBinding(*cli, cli_cmd_def_led);
    embeddedCliAddBinding(*cli, cli_cmd_def_param_table);
    embeddedCliAddBinding(*cli, cli_cmd_def_get_param);
    embeddedCliAddBinding(*cli, cli_cmd_def_set_param);
 
    return EXIT_SUCCESS;
}


void cli_on_unkown_cmd(EmbeddedCli *embeddedCli, CliCommand *command) {
    Serial.print(F("Unknown command:"));
    Serial.println(command->name);
    Serial.println(F("Type 'help' to see all available commands...\n"));
}

void cli_write_char(EmbeddedCli *embeddedCli, char c) {
    Serial.write(c);
}