// BSP includes
#include <Arduino.h>
// External includes
#include "embedded_cli.h"
#include "s3/param.h"
// Project includes
#include "sys/sys_calls.h"

void cli_cmd_fun_led_toggle(EmbeddedCli *cli, char *args, void *context);
void cli_cmd_fun_exit_cli(EmbeddedCli *cli, char *args, void *context);


CliCommandBinding cli_cmd_def_led = {
    "led",
    "Toggle led status",
    false,
    NULL,
    cli_cmd_fun_led_toggle
};


CliCommandBinding cli_cmd_def_exit_cli = {
    "exit",
    "Exit CLI and return to Tele-Command mode",
    true,
    NULL,
    cli_cmd_fun_exit_cli
};


void cli_cmd_fun_led_toggle(EmbeddedCli *cli, char *args, void *context) 
{
    static bool led_status = HIGH;
    led_status = !led_status;
    digitalWrite(LED_BUILTIN, led_status); 
    Serial.print(F("LED: "));
    Serial.print(led_status == HIGH? "ON" : "OFF");
    Serial.print("\r\n");
}
