// BSP includes
#include "Arduino.h"
// External includes
#include "embedded_cli.h"
// Project includes
#include "s3/param.h"
#include "utils/param_utils.hpp"


// Reference to root param table
extern param_table_t root_table;


void cli_cmd_print_param_table(EmbeddedCli *cli, char *args, void *context);
void cli_cmd_set_param(EmbeddedCli *cli, char *args, void *context);
void cli_cmd_get_param(EmbeddedCli *cli, char *args, void *context);

CliCommandBinding cli_cmd_def_param_table = {
    "table",
    "Print table of Parameters",
    false,
    NULL,
    cli_cmd_print_param_table
};


CliCommandBinding cli_cmd_def_set_param = {
    "set",
    "Set the value of a Parameter:set <param> <value>",
    true,
    NULL,
    cli_cmd_set_param
};


CliCommandBinding cli_cmd_def_get_param = {
    "get",
    "Get the value of a Parameter: get <param>",
    true,
    NULL,
    cli_cmd_get_param
};





void cli_cmd_print_param_table(EmbeddedCli *cli, char *args, void *context)
{
  print_param_table();
}


void cli_cmd_set_param(EmbeddedCli *cli, char *args, void *context) 
{
  char buff[32];
  param_ref_t* ref;
  if (embeddedCliGetTokenCount(args) != 2)
  {
    Serial.println("Wrong command usage. Type 'help' to see usage.");
    return;
  }
  if((ref = query_table(&root_table, embeddedCliGetToken(args, 1))) == NULL)
  {
    Serial.print("Param with name '");
    Serial.print(embeddedCliGetToken(args, 1));
    Serial.println("' not found.");
    return;
  }
  // Set the value
  pref_from_str(ref, embeddedCliGetToken(args, 2), strlen(embeddedCliGetToken(args, 2)));

  Serial.print(embeddedCliGetToken(args, 1));
  Serial.print(": ");
  pref_to_str(ref, buff, sizeof(buff));
  Serial.println(buff);
}


void cli_cmd_get_param(EmbeddedCli *cli, char *args, void *context) 
{
  char buff[32];
  param_ref_t* ref;
  if (embeddedCliGetTokenCount(args) != 1)
  {
    Serial.println("Wrong command usage. Type 'help' to see usage.");
    return;
  }
  if((ref = query_table(&root_table, embeddedCliGetToken(args, 1))) == NULL)
  {
    Serial.print("Param with name '");
    Serial.print(embeddedCliGetToken(args, 1));
    Serial.println("' not found.");
    return;
  }

  Serial.print(embeddedCliGetToken(args, 1));
  Serial.print(": ");
  pref_to_str(ref, buff, sizeof(buff));
  Serial.println(buff);
}
