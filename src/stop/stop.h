/**
 * @file stop.h
 * 
 * Stored Test and Operation Procedures (STOP)
 * 
 * STOP stands for Stored Test and Operation Procedures. The purpose is to 
 * execute long or complex procedures, those that can take longer than an few 
 * millis, or those which require cycles or steps to complete.
 * 
 * Since our platform runs on a single thread (no multi thread or Operating 
 * System). The idea is to have a way to execute procedures meanwhile the state 
 * machine is running. Otherwise, if a procedure blocks the processor, can 
 * degrade the performance of the whole system and provoked exceptions (eg. 
 * watchdog triggers, buffer overflows, etc). STOPs library addresses this 
 * issue. User can define STOP routines and ask a STOP runner to run the 
 * routine. The STOP runner has to be called every state machine cycle, so the
 * active STOP routine will run a cycle. 
 * 
 * The rule of thumb for writing STOP routines is to never use a cycle 
 * (while or for loop) inside it. And to achieve the cyclic execution whit the 
 * sequential invocation of the STOP routine.
 * 
 * The STOP library is used for example for operations with files or external 
 * devices. For example a STOP for reason a file, will read a single line every 
 * call.
 * 
 */


#ifndef STOP_H_
#define STOP_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Return status of STOP routine.
 * 
 */
typedef enum
{
    STOP_DONE,           ///< STOP routine completed. Return to finish STOP execution.
    STOP_IN_PROGRESS,    ///< STOP routine in progress. Return to continue execution of STOP.
    STOP_FAILED          ///< STOP routine failed. Return to exit STOP execution.
} stop_status_t;



/**
 * @brief Context of STOP routine. 
 * 
 * Is the argument of a STOP routine.
 * 
 */
typedef struct 
{
 size_t cycle_cnt;    ///< Incremental counter representing the curent cycle.
 void*  args;         ///< Argument of specific STOP routine. Given when runner is called.
} stop_ctx_t;



/**
 * @brief  Definition of STOP routine.
 *  
 */
typedef stop_status_t (*stop_function_t)(stop_ctx_t* ctx);



/**
 * @brief STOP runner.
 * 
 * The STOP runner is the unit in charge of executing the STOP routines 
 * requested by the user. It has to be invoked once per main loop cycle  with
 * @see stop_runner_work() 
 * 
 */
typedef struct
{
    size_t          cycle_cnt;
    stop_function_t curr_stop;
    void*           stop_args;
} stop_runner_t;


/**
 * @brief Return status of STOP runner
 * 
 */
typedef enum
{
    RUNNER_OK = 0,
    RUNNER_FAILURE,

    STOP_FAILURE,

    STOP_ACCEPTED,
    RUNNER_BUSY
} stop_runner_status_t;



/**
 * @brief Request a STOP to be executed
 * 
 * @note The runner has to be available. 
 * 
 * @param runner 
 * @param stop 
 * @param stop_args 
 * @return stop_runner_status_t 
 */
stop_runner_status_t stop_run(stop_runner_t* runner, stop_function_t stop, void* stop_args);




/**
 * @brief Initialize a STOP runner
 * 
 * @param runner 
 * @return stop_runner_status_t 
 */
stop_runner_status_t stop_runner_init(stop_runner_t* runner);

/**
 * @brief Execute a cycle of a STOP Runner. 
 * 
 * @note Call this function each main loop cycle to execute pending STOPs.
 * 
 * @param runner 
 * @return stop_runner_status_t 
 */
stop_runner_status_t stop_runner_work(stop_runner_t* runner);







#ifdef __cplusplus
}
#endif
#endif // end STOP_H_