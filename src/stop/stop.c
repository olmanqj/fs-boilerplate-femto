#include <assert.h>

#include "stop/stop.h"



stop_runner_status_t stop_runner_init(stop_runner_t* runner)
{
    assert(runner);
    if(runner == NULL) return RUNNER_FAILURE;
    runner->curr_stop = NULL;
    runner->cycle_cnt = 0;
    return RUNNER_OK;
}



stop_runner_status_t stop_runner_work(stop_runner_t* runner)
{
    assert(runner);
    stop_status_t status;
    stop_ctx_t ctx;

    if(runner == NULL) return RUNNER_FAILURE;

    // Check if active STOP
    if(runner->curr_stop == NULL) return RUNNER_OK;
    // Setup context for STOP
    ctx.cycle_cnt = runner->cycle_cnt;
    ctx.args = runner->stop_args;
    // Run the STOP
    status = runner->curr_stop(&ctx);
    switch (status)
    {
    case STOP_DONE:
        // If done, remove the active STOP
        runner->curr_stop = NULL;
        runner->cycle_cnt = 0;
        runner->stop_args = NULL;
        break;
    case STOP_IN_PROGRESS:
        // If in Progress, just increment cycle count
        runner->cycle_cnt++;
        break;
    case STOP_FAILED:
    default:
        // IF failed or code unknown, return Failure
        runner->curr_stop = NULL;
        runner->cycle_cnt = 0;
        runner->stop_args = NULL;
        return STOP_FAILED;
    }
    return RUNNER_OK;
}



// TODO better handling of args, stop will begin executing after stop_run returns, lifetime of args may be expired 
stop_runner_status_t stop_run(stop_runner_t* runner, stop_function_t stop, void* stop_args)
{
    assert(runner); assert(stop);
    if(runner == NULL || stop == NULL) return RUNNER_FAILURE;

    // If runner busy, return Failure
    if(runner->curr_stop != NULL)
        return RUNNER_BUSY;
    
    runner->curr_stop = stop;
    runner->cycle_cnt = 0;
    runner->stop_args = stop_args;

    return STOP_ACCEPTED;
}

