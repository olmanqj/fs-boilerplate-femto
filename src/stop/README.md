# Stored Tests and Operations Procedures (STOP)

STOP stands for Stored Test and Operation Procedures. The purpose is to execute long or complex procedures, those that can take longer than an few millis, or those which require cycles or steps to complete.

Since our platform runs on a single thread (no multi thread or Operating System). The idea is to have a way to execute procedures meanwhile the state machine is running. Otherwise, if a procedure blocks the processor, can degrade the performance of the whole system and provoked exceptions (eg. watchdog triggers, buffer overflows, etc)
STOPs library addresses this issue. User can define STOP routines and ask a STOP runner to run the routine. The STOP runner has to be called every state machine cycle, so the active STOP routine will run a cycle.
The rule of thumb for writing STOP routines is to never use a cycle (while or for loop) inside it. And to achieve the cyclic execution whit the sequential invocation of the STOP routine.

The STOP library is used for example for operations with files or external devices. For example a STOP for reason a file, will read a single line every call.