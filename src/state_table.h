#include "s3/statem.h"
#include "modes/modes.hpp"
#include "config.h"

state_def_t state_table[] = {
{.code=Idle,            .name="idle",           .routine=&idle_routine,             .transition=nullptr, .resume=nullptr,          .pause=nullptr},
{.code=Self_test,       .name="test",           .routine=&self_test_routine,        .transition=nullptr, .resume=nullptr,          .pause=nullptr},
{.code=Science,         .name="science",        .routine=&science_routine,          .transition=nullptr, .resume=nullptr,          .pause=nullptr},
{.code=Sleep,           .name="sleep",          .routine=&safe_routine,            .transition=nullptr, .resume=&safe_on_resume, .pause=&safe_on_pause}
};



size_t state_table_size = sizeof(state_table)/sizeof(state_def_t);