/**
 * @file telemetry_serializer.h
 * 
 * @author olmanqj
 * @brief This file defines the telemetry Serializer Function. The function
 * is used to collect telemetry parameters and format them as required for
 * transmission. 
 * @date 2023-07-31
 *
 */

#ifndef PARAMETERS_TELEMETRY_SERIALIZER_H_
#define PARAMETERS_TELEMETRY_SERIALIZER_H_
#ifdef __cplusplus
extern "C" {
#endif


int telemetry_serializer(char * dest_buff, size_t buff_size);


#ifdef __cplusplus
}
#endif
#endif /* PARAMETERS_TELEMETRY_SERIALIZER_H_ */
