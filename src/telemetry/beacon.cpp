/*

// BSP includes
#include <Arduino.h>
// External includes
#include "arduino-timer.h"
#include "s3/param.h"
// Project includes

extern paramU8_t  tlm_ena;
extern paramU32_t tlm_count;
extern paramU32_t tlm_period;


Timer<>::Task beacon_task;

bool  task_beacon_fn(void* args) // TODO
{
    if(getUINT8(&tlm_ena))
    {
        Serial.println("[BEACON]> Sending periodic beacon");
        // Increment count
        setUINT32(&tlm_count, getUINT32(&tlm_count) + 1 );
    }
    return true;
}


void init_beacon_task(Timer<>& timer, uint32_t period)
{
    beacon_task = timer.every(getUINT32(&tlm_period), task_beacon_fn);
}

void stop_beacon_task(Timer<>& timer)
{
    if(beacon_task)
        timer.cancel(beacon_task);
}

void resume_beacon_task(Timer<>& timer, uint32_t period)
{
    if(beacon_task)
        timer.cancel(beacon_task);
    beacon_task = timer.every(getUINT32(&tlm_period), task_beacon_fn);
}

*/