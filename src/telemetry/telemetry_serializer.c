#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <stdbool.h>

#include "s3/param.h"
#include "telemetry_serializer.h"

/**
 * This is the custom function which will determine the format of a telemetry frame
 *
 */


int telemetry_serializer(char * dest_buff, size_t buff_size)
{

	// static param_handle_t curr_param; // Static to keep track for next call, in order to support pagination
	// static bool pending;
	// int tag_id;
	// // buff to store param value as str
	// char param_buff[CONF_PARAM_MAX_PARAM_SIZE];
	// tag_id=0;

	// #ifndef ENABLE_TELEMETRY_PAGINATION
	// // Before iterate over table; reset iterator
	// rewind_param_table_iterator();
	// #endif

	// // If starting again from the beginning, get the first param
	// if(!pending)
	// 	curr_param=param_table_iterator();
	// // Reset the flag
	// pending = false;

	// // Iterate over table to get next param handle
	// while(curr_param!=NULL)
	// {
	// 	// Clear buffers
	// 	memset(param_buff, 0, sizeof(param_buff));
	// 	// Store the value from param as str
	// 	param_to_str(curr_param, param_buff, sizeof(param_buff));
	// 	// Check if enough space in dest_buff (enough for tag + param + ':' + ',')
	// 	if(strlen(dest_buff)+strlen(curr_param->name)+strlen(param_buff)+2 >= buff_size)
	// 	{
	// 		pending = true;
	// 		break;
	// 	}
	// 	// Separate by comma, except if first
	// 	if(strlen(dest_buff) != 0) strcat(dest_buff,   "," );
	// 	// Concatenate  TAG
	// 	strcat(dest_buff,  curr_param->name );
	// 	// Concatenate ':' separator
	// 	strcat(dest_buff, ":");
	// 	// Concatenate param value
	// 	strcat(dest_buff, param_buff);
	// 	tag_id++;
	// 	// Advance to next param
	// 	curr_param=param_table_iterator();
	// }
	return strlen(dest_buff);
}


