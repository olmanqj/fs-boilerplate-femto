#ifndef PARAM_TABLE_H_
#define PARAM_TABLE_H_

#ifdef __cplusplus
extern "C" {
#endif


#include "s3/param.h"

#include "sys/param_callbacks.h"

//							NAME			Value	Options							Comment
/*******************************************************************************
 * 		System Params
 *******************************************************************************/
paramU8_t  sys_mode      = {"mode",			    1, 		     .callback=&on_sys_mode_param_change}; // Operation mode of system
paramU16_t sys_error 	 = {"error",            0};								                   // Error registry



/*******************************************************************************
 * 		Telemetry
 *******************************************************************************/       
paramU8_t  tlm_ena       = {"tlm_ena",          1};	                                                        // Enable/Disable telemetry beacon
paramU32_t tlm_count 	 = {"tlm_count",		0};						                                    // HK Service, counts the amount of telemetry frames sent
paramU32_t tlm_period 	 = {"tlm_period",		10000,			.callback=&on_tlm_period_param_change};		// HK Service, the period between each telemetry frame transmission



/**
 * @brief	Parameter Table
*/

param_list_t root_param_list = {
	// System Params
	UINT8_REF (sys_mode),
	UINT16_REF(sys_error),


	// Telemetry
	UINT8_REF (tlm_ena),
	UINT32_REF(tlm_count),
	UINT32_REF(tlm_period),
};



param_table_t root_table = {"root", root_param_list, sizeof( root_param_list)/sizeof(*root_param_list)};



#ifdef __cplusplus
}
#endif
#endif /* PARAM_TABLE_H_ */
