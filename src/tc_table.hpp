#include "tc/tc.hpp"
#include "tc/tc_routines.h"




const tc_table_t tc_table = {
    {.code = 0x01, .fn = tc_f_dummy},                    // Dummy command
    {.code = 0x02, .fn = tc_f_idle_mode},                // Switch to Idle mode
    {.code = 0x03, .fn = tc_f_scientific_mode},          // Switch to Scientific mode
    {.code = 0x04, .fn = tc_f_param_table},              // Print the Param Table
    {.code = 0x05, .fn = tc_f_set_param},                // Set value of Param
    {.code = 0x06, .fn = tc_f_get_param},                // Get value of Param
};



size_t tc_table_size = sizeof(tc_table)/sizeof(*tc_table);